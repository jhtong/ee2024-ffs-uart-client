# ee2024-ffs-uart-client

[![Dependency status](https://david-dm.org/myrtleTree33/ee2024-ffs-uart-client.png)](https://david-dm.org/myrtleTree33/ee2024-ffs-uart-client)
[![devDependency Status](https://david-dm.org/myrtleTree33/ee2024-ffs-uart-client/dev-status.png)](https://david-dm.org/myrtleTree33/ee2024-ffs-uart-client#info=devDependencies)
[![Build Status](https://secure.travis-ci.org/myrtleTree33/ee2024-ffs-uart-client.png?branch=master)](https://travis-ci.org/myrtleTree33/ee2024-ffs-uart-client)

[![NPM](https://nodei.co/npm/ee2024-ffs-uart-client.png?downloads=true)](https://npmjs.org/package/ee2024-ffs-uart-client)

## Installation

    npm install ee2024-ffs-uart-client

## Usage Example

## Testing

    npm test

## License

The MIT License (MIT)

Copyright 2014 jhtong

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.