serialport = require('serialport')
SerialPort = serialport.SerialPort 

sp = new SerialPort '/dev/ttyUSB0', {
    baudrate : 115200,
    parser: serialport.parsers.readline("\n")
}

ackCtr = 0
auth = false;

procAuth = (tokens) ->

    return if not tokens

    if tokens[0] is 'RDY' and ackCtr isnt 3
        sp.write 'RNACK\r'
        ackCtr++

    else if tokens[0] is 'RDY' and ackCtr is 3
        sp.write 'RACK\r'

    else if tokens[0] is 'HSHK' 
        console.log 'reached hshk'
        auth = true

## main body

sp.open ->
    console.log 'open'
    sp.write 'test string\r\n', (err, results) ->
        console.log 'done'

    sp.on 'data', (data) ->
        console.log "Data: #{data}"
        tokens = data.split(' ').slice(0, -1)
        console.log tokens                      # dump

        return procAuth(tokens) if not auth

counter2 = false

doActiveStuff = ->
    console.log 'ping!'
    counter2 = !counter2
    
    if counter2 then sp.write 'RSTS\r'
    else sp.write 'RSTC\r'

setInterval ->
    doActiveStuff() if auth;
, 3000






console.log 'done'

