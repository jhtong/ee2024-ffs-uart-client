require 'coffee-errors'

chai = require 'chai'
sinon = require 'sinon'
# using compiled JavaScript file here to be sure module works
ee2024-ffs-uart-client = require '../lib/ee2024-ffs-uart-client.js'

expect = chai.expect
chai.use require 'sinon-chai'

describe 'ee2024-ffs-uart-client', ->
  it 'works', ->
    actual = ee2024-ffs-uart-client 'World'
    expect(actual).to.eql 'Hello World'
